<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Thulium\Block\Thulium;

class Add extends \Magento\Framework\View\Element\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context    $context,
        \Kowal\Thulium\Model\Config\Source\Tags             $tags,
        \Kowal\Thulium\Model\Config\Source\TicketCategories $ticketCategories,
        \Kowal\Thulium\Helper\Config                        $config,
        array                                               $data = []
    )
    {
        $this->tags = $tags;
        $this->config = $config;
        $this->ticketCategories = $ticketCategories;
        parent::__construct($context, $data);
    }

    public function getTags()
    {
        return $this->tags->toArray();
    }

    public function getCategories()
    {
        return $this->ticketCategories->toArray();
    }

    public function getAddress(){
        return nl2br((string)$this->config->getGeneralCfg('adres_zwrotu'));
    }

    public function getTitle(){
        return nl2br((string)$this->config->getGeneralCfg('form_title'));
    }

    public function getDescription(){
        return nl2br((string)$this->config->getGeneralCfg('form_description'));
    }
    public function getOdstapienie(){
        return (string)$this->config->getGeneralCfg('ticket_categories');
    }

    public function getFooter(){
        if($footer = $this->config->getGeneralCfg('form_footer_description')) {
            if( !is_array($footer)) {
                return nl2br((string)$footer);
            }else{
                return '';
            }
        }else{
            return "";
        }
    }
}

