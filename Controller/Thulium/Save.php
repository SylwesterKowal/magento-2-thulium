<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Thulium\Controller\Thulium;

use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Response\Http;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\View\Result\PageFactory;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\RequestInterface;
use Kowal\Thulium\Helper\Config;
use Kowal\Thulium\Helper\Request;

class Save implements HttpPostActionInterface
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var Json
     */
    protected $serializer;
    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @var Http
     */
    protected $http;

    /**
     * Constructor
     *
     * @param PageFactory $resultPageFactory
     * @param Json $json
     * @param LoggerInterface $logger
     * @param Http $http
     */
    public function __construct(
        PageFactory      $resultPageFactory,
        Json             $json,
        LoggerInterface  $logger,
        Http             $http,
        RequestInterface $request,
        Config           $config,
        Request          $requestThulium
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->serializer = $json;
        $this->logger = $logger;
        $this->http = $http;
        $this->request = $request;
        $this->config = $config;
        $this->requestThulium = $requestThulium;
    }

    /**
     * Execute view action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        try {

//            $request = $this->request->getPostValue();
//             return $this->jsonResponse(print_r($this->validatedParams(),true));

            $response = $this->requestThulium->send($this->validatedParams(), 'POST', '/tickets');
            $ticket_id = (isset($response['ticket_id'])) ? $response['ticket_id'] : "Błąd zapisu zlecenia!: " . print_r($response, true);
            return $this->jsonResponse($ticket_id);

        } catch (LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Create json response
     *
     * @return ResultInterface
     */
    public function jsonResponse($response = '')
    {
        $this->http->getHeaders()->clearHeaders();
        $this->http->setHeader('Content-Type', 'application/json');
        return $this->http->setBody(
            $this->serializer->serialize($response)
        );
    }

    private function validatedParams()
    {
        $request = $this->request->getPostValue();

        if (trim($request['firstname']) === '') {
            throw new LocalizedException(__('Wprowadź imię i spróbuj ponownie.'));
        }
        if (trim($request['lastname']) === '') {
            throw new LocalizedException(__('Wprowadź nazwisko i spróbuj ponownie.'));
        }
        if (trim($request['kodpocztowy']) === '') {
            throw new LocalizedException(__('Wprowadź kod pocztowy i spróbuj ponownie.'));
        }
        if (trim($request['miejscowosc']) === '') {
            throw new LocalizedException(__('Wprowadź miejscowosc i spróbuj ponownie.'));
        }
        if (trim($request['adres']) === '') {
            throw new LocalizedException(__('Wprowadź adres i spróbuj ponownie.'));
        }
        if (trim($request['body']) === '') {
            throw new LocalizedException(__('Wprowadź powód zgłoszenia i spróbuj ponownie.'));
        }

        if (trim($request['phone']) === '') {
            throw new LocalizedException(__('Wprowadź telefon i spróbuj ponownie.'));
        }

        if (trim($request['tags'][0]) === '') {
            throw new LocalizedException(__('Wprowadź produkt zgłoszenia i spróbuj ponownie.'));
        }
        if (\strpos($request['email'], '@') === false) {
            throw new LocalizedException(__('Adres e-mail jest nieprawidłowy. Wprowadź poprawny adres e-mail i spróbuj ponownie.'));
        }
        if (trim($request['hideit']) !== '') {
            // phpcs:ignore Magento2.Exceptions.DirectThrow
            throw new \Exception();
        }

//                $data = '{"to":"kontakt@kowal.co","subject":"ticket subject","from":"orllo@orllo.pl","body":"email content","comment":"ticket comment","cc":["orllo@orllo.pl"]}';
        $data = [
            "to" => $this->config->getGeneralCfg('email_odbiorcy'),
            "subject" => "Zgłoszenie ID# ",
            "category_id" => (string)$request['category'],
            "tags" => $this->getTags($request),
            "from" => $request['email'],
            "body" => PHP_EOL . $this->getBodyName($request) . PHP_EOL . $this->getBodyAdes($request) . PHP_EOL. $this->getBodyRachunek($request) . PHP_EOL . $this->getBodyPhone($request) . PHP_EOL . $this->getBodyTags($request) . PHP_EOL . $this->getBodyOdsylaneElementy($request) . PHP_EOL . 'Kategoria: ' . $request['category_name'],
            "comment" => $request['body'],
            "cc" => [$this->config->getGeneralCfg('email_dw')]
        ];


        return $data;
    }

    private function getBodyPhone($request)
    {
        $phone = 'Telefon: ';
        if (isset($request['phone'])) {
            $phone .= $request['phone'] . PHP_EOL;
        }
        return $phone;
    }

    private function getBodyRachunek($request)
    {
        $rachunek = 'Rachunek Bankowy: ';
        if (isset($request['rachunek'])) {
            $rachunek .= $request['rachunek'] . PHP_EOL;
        }
        return $rachunek;
    }

    private function getBodyName($request)
    {
        $name = '';
        if (isset($request['firstname'])) {
            $name .= $request['firstname'];
        }
        if (isset($request['lastname'])) {
            $name .= " " . $request['lastname'];
        }
        return $name;
    }

    private function getBodyAdes($request)
    {
        $adres = '';
        if (isset($request['kodpocztowy'])) {
            $adres .= $request['kodpocztowy'];
        }
        if (isset($request['miejscowosc'])) {
            $adres .= " " . $request['miejscowosc'];
        }
        if (isset($request['adres'])) {
            $adres .= PHP_EOL . $request['adres'];
        }
        return $adres;
    }


    private function getBodyTags($request)
    {
        $tags = '';
        if (isset($request['tags'])) {
            foreach ($request['tags'] as $key => $sku) {
                $tags .= $sku . ' - ' . $request['ilosc'][$key] . ' szt.' . PHP_EOL;
            }
        }
        return $tags;
    }

    private function getBodyOdsylaneElementy($request)
    {
        $odsylane = 'Odsyłane elementy: ' . PHP_EOL;
        if (isset($request['karta_sd']) && $request['karta_sd'] == 1) {
            $odsylane .= 'KARTA SD ' . PHP_EOL;
        }
        if (isset($request['karta_sim']) && $request['karta_sim'] == 1) {
            $odsylane .= 'KARTA SIM ' . PHP_EOL;
        }
        if (isset($request['inne']) && $request['inne'] == 1) {
            $odsylane .= 'INNE : ' . $request['opis'] . PHP_EOL;
        }
        return $odsylane;
    }

    private function getTags($request)
    {
        $tags = [];
        if (isset($request['tags'])) {
            foreach ($request['tags'] as $key => $sku) {
                $tags[] = $sku;
            }
        }
        return $tags;
    }


}

