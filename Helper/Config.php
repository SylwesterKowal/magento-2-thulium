<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Thulium\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Config extends AbstractHelper
{

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return true;
    }

    const SECTIONS = 'thulium';   // module name
    const GROUPS = 'settings';        // setup general

    public function getConfig($cfg = null)
    {
        return $this->scopeConfig->getValue(
            $cfg,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getGeneralCfg($cfg = null)
    {
        $config = $this->scopeConfig->getValue(
            self::SECTIONS . '/' . self::GROUPS,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        if (isset($config[$cfg])) return $config[$cfg];
        return $config;
    }
}
