<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Thulium\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\Serializer\Json;

class Request extends AbstractHelper
{

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Kowal\Thulium\Helper\Config          $config
    )
    {
        $this->config = $config;
        parent::__construct($context);
    }

    /**
     * @param $data
     * @param $request_type
     * @return mixed|void
     */
    public function send($data, $request_type = 'POST', $endpoint = '/tickets')
    {
        // return print_r($data,true);


        $username = $this->config->getGeneralCfg('username');
        $password = $this->config->getGeneralCfg('password');
        $api_url = $this->config->getGeneralCfg('api_url');

        $request = curl_init($api_url . $endpoint);
        if ($data) {
            curl_setopt($request, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json; charset=utf-8', 'Content-Length: ' . mb_strlen(json_encode($data))
            ));
        } else {
            curl_setopt($request, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
        }
        curl_setopt($request, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($request, CURLOPT_TIMEOUT, 90);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($request, CURLOPT_CUSTOMREQUEST, $request_type);
        if ($data) curl_setopt($request, CURLOPT_POSTFIELDS, json_encode($data));

        $response = curl_exec($request);
        if ($response) {
            $httpCode = curl_getinfo($request, CURLINFO_HTTP_CODE);
            if ($httpCode == 200 || $httpCode == 201) {
                return json_decode($response, true);
            } else {
                throw new LocalizedException(__($response . ": " . $httpCode));
            }
        } else {
            trigger_error(curl_error($request), E_USER_WARNING);
        }

        curl_close($request);
    }
}
