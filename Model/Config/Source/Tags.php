<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Thulium\Model\Config\Source;

class Tags implements \Magento\Framework\Option\ArrayInterface
{
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Psr\Log\LoggerInterface                                       $logger
    )
    {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->logger = $logger;
    }

    public function toOptionArray()
    {
        $arrayOptions = [];
        $productCollection = $this->getProductCollection();
        foreach ($productCollection as $product) {
            $arrayOptions[] = ['value' => $product->getSku(), 'label' => $product->getSku()];
        }
        return $arrayOptions;
    }

    public function toArray()
    {
        $arrayValues = [];
        if($options = $this->toOptionArray()) {
            foreach ($options as $option) {
                $arrayValues[$option['value']] =  $option['label'];
            }
        }
        return $arrayValues;
    }


    public function getProductCollection()
    {
        $collection = $this->productCollectionFactory->create();
        $collection->addAttributeToSelect('sku');
        $collection->addAttributeToSelect('name');
        $collection->setOrder('sku', 'ASC');
        return $collection;
    }

}