<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Thulium\Model\Config\Source;

class TicketCategories implements \Magento\Framework\Option\ArrayInterface
{
    public function __construct(
        \Kowal\Thulium\Helper\Request $request,
        \Psr\Log\LoggerInterface      $logger
    )
    {
        $this->requestThulium = $request;
        $this->logger = $logger;
    }


    public function toOptionArray()
    {
        $categories = [];
        $categories_ = $this->getCategories();
        foreach ($categories_ as $cat) {
            $categories[] = ['value' => $cat['id'], 'label' => $cat['name']];
        }
        return $categories;
    }

    public function toArray()
    {
        $arrayValues = [];
        if($options = $this->toOptionArray()) {
            foreach ($options as $option) {
                $arrayValues[$option['value']] =  $option['label'];
            }
        }
        return $arrayValues;
    }

    private function getCategories(){
        try{
        if($categories = $this->requestThulium->send(null,'GET','/ticket_categories')){
            if(is_array($categories)){
                return $categories;
            }else{
                return [];
            }

        }else{
            return [];
        }
        } catch (LocalizedException $e) {
//            return $this->jsonResponse($e->getMessage());
            return [];
        } catch (\Exception $e) {
            $this->logger->critical($e);
//            return $this->jsonResponse($e->getMessage());
            return [];
        }
    }

}

